# Welcome to ku airmail

I learn about ifttt. 
I am interested in automation and integration of various services. 

## Wordpress 

Come take a look at my wordpress blog 
[https://kuairmail.wordpress.com](https://kuairmail.wordpress.com). 

## Twitter 

My twitter handle is @kuairmail 

## Email 

ku@airmail.cc 

`508E 82FA F59E E8D6 BE8F 6855 D61F 389C DCB8 03F3` 

## Github 

[https://github.com/kuairmail](https://github.com/kuairmail)

## Anything I missed? 

Submit a pull request on gitlab! 
https://gitlab.com/kuairmail/kuairmail.gitlab.io/


## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
